# NoSQl Database 
NoSQL database is a type of database that stores data in document form rather than in tabular form. The advantage of a NoSQL database over a relational database is that it is easily scaled horizontally, which means we can add more servers to store the data. To get data from the server we need a primary key, using which we can get value with the help of the **Hash function**.

These are the most common NoSQL databases:
## MongoDB

MongoDB is the most popular NoSQL database in use today. It uses "**documents**" like **JSON** instead of using relational schemas to store the data. MongoDB also provides features of indexing, load balancing, querying, and replication. The main reason for using the MongoDB database is that it is free and open source and works efficiently on Big Data. It supports ad-hoc queries and is based on schemaless design.
## Cassandra
Cassandra is a distributed database management system designed to handle high volumes of structured data in servers. Cassandra provides low latency client access and it is also free and open source. It supports asynchronous updating and master-less design, Cassandra is a **column-oriented database**, which means its rows contain what we most usually think of as vertical data. The advantage of the column-oriented design is that some types of data lookups can become very fast. It is also an optimized and decentralized model.
## Redis
Redis (Remote Dictionary Server) is a NoSQL database that stores data in a **key-value** implementation. In key-value implementation, each key is assigned to a particular value. Operations in Redis are atomic and it supports a wide variety of data types, **Lua scripting**, and **multi-utility tools**. It replicates data to any number of slaves and is very fast and easy to set up.
## HBase
HBase is a free, open source, distributed, and column-oriented database designed by Google for the BigTable database. We can add servers anytime to increase the capacity, it is closely associated with **Hadoop**, as it is part of the Apache project. It provides fast lookup for large tables and provides low latency access from billions of records to single rows. It is license-free and supports easy Java API for clients, and auto-sharing.
## Neo4j
Neo4j is referred to as the basic **graph database** because it stores data using nodes and edges. 

There are two versions of the database in Neo4j.
1. Community edition
1. Enterprise Edition

The Enterprise edition provides all the features that the Community edition provides, but it offers additional capabilities such as backup, clustering, and failover.

Neo4j supports unique constraints, Cypher API and native Java API, and easy query language Neo4j CQL. It contains UI for executing CQL commands (Neo4j Data Browser). It represents semi-structured data very easily and does not require complex joins to retrieve the data.

## References 

* [https://www.youtube.com/watch?v=0buKQHokLK8](https://www.youtube.com/watch?v=0buKQHokLK8)

* [https://www.mongodb.com/nosql-explained](https://www.mongodb.com/nosql-explained)

* [https://ubuntu.com/blog/nosql-databases-what-is-mongodb-and-its-use-cases](https://ubuntu.com/blog/nosql-databases-what-is-mongodb-and-its-use-cases)

* [https://www.kdnuggets.com/2016/06/top-nosql-database-engines.html](https://www.kdnuggets.com/2016/06/top-nosql-database-engines.html)

* [https://www.guru99.com/cassandra-tutorial.html](https://www.guru99.com/cassandra-tutorial.html)